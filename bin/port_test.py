from __future__ import print_function

import json
import optparse
import sys
import threading

from port_tester_utils import port_tester_utils_parse
from port_tester_utils import port_tester_utils_log_result
from port_tester_utils import port_tester_utils_base_log

THREAD_MAX = 10
THREAD_GAP = 0.25


def spinster(url, between=None):
    addr, port, scheme = port_tester_utils_parse.parse(url)

    if between is None:
        return port_tester_utils_log_result.log_result(addr, False, 5, port, 0.0, scheme)
    else:
        if '-' in between:
            lo = int(between.split('-')[0])
            hi = int(between.split('-')[1])
            do = [lo + x for x in range(hi - lo + 1)]
        elif ',' in between:
            do = list(set([int(x) for x in between.split(',')]))
            do.sort()
        else:
            return port_tester_utils_log_result.log_result(addr, False, 5, None, 0.0, scheme)

        delay = 0.0
        spool = [None] * THREAD_MAX
        index = 0

        for it in do:
            if spool[index] is None:
                spool[index] = [
                    threading.Thread(
                        target=port_tester_utils_log_result.log_result,
                        args=(addr, False, 5, it, delay, scheme),
                        name='port_%d' % it
                    )
                ]
            else:
                spool[index].append(
                    threading.Thread(
                        target=port_tester_utils_log_result.log_result,
                        args=(addr, False, 5, it, delay, scheme),
                        name='port_%d' % it
                    )
                )
            index = (index + 1) % THREAD_MAX
            if index == 0:
                delay += THREAD_GAP

        for thread in spool:
            if thread is None:
                continue
            for fiber in thread:
                fiber.start()


parser = optparse.OptionParser(description='Port Tester')
parser.add_option('-u', '--url', action='store', dest='url', help='URL:port (example: "http://www.google.com:80"')
parser.add_option('-r', '--range', action='store', dest='range', help='Optional Port Range (example: "100-255")')
parser.add_option('-p', '--pretty', action='store_true', dest='pretty', help='Print Indented JSON')
parser.add_option('-t', '--thread', action='store', type='int', dest='thread', help='Override Thread Max')
parser.add_option('-s', '--spread', action='store', type='float', dest='spread', help='Override Threat Time Separation')
parser.add_option('-l', '--limit', action='store', type='float', dest='limit', help='Override Socket Timeout Limit')
(cli_options, args) = parser.parse_args()

using = cli_options.__dict__

if using['thread'] is not None:
    THREAD_MAX = using['thread']

if using['spread'] is not None:
    THREAD_GAP = using['spread']

if using['limit'] is not None:
    port_tester_utils_log_result.set_socket_timeout(using['limit'])

if using['url'] is None:
    args = [
        '-u',
        '--url',
        '-r',
        '--range',
        '-p',
        '--pretty',
        '-t',
        '--thread',
        '-s',
        '--spread',
        '-l',
        '--limit'
    ]
    if len(sys.argv) > 1 and sys.argv[1] not in args:
        using['url'] = sys.argv[1]

if len(sys.argv) > 1:
    try:
        if using['range'] is not None:
            spinster(using['url'], using['range'])
        elif using['url'] is not None:
            spinster(using['url'])
        else:
            base_log = port_tester_utils_base_log.generate()
            base_log.update({
                'error': 'Script arguments could not be parsed',
                'arguments': sys.argv
            })
            if '-p' in sys.argv:
                print(json.dumps(base_log, sort_keys=True, indent=4))
            else:
                print(json.dumps(base_log, sort_keys=True))
    except Exception as e:
        import traceback
        error = traceback.format_exc(e)
        base_log = port_tester_utils_base_log.generate()
        base_log.update({
            'error': 'Script arguments could not be parsed',
            'traceback': error
        })
        if '-p' in sys.argv:
            print(json.dumps(base_log, sort_keys=True, indent=4))
        else:
            print(json.dumps(base_log, sort_keys=True))
