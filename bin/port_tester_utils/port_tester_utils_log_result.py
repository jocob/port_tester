import json
import sys

from port_tester_utils_base_log import generate
from port_tester_utils_one_two import one_two, set_socket_timeout as one_two_timeout


SOCKET_TIMEOUT = 1.0


def set_socket_timeout(value):
    global SOCKET_TIMEOUT
    SOCKET_TIMEOUT = value
    one_two_timeout(value)


def log_result(url, verbose=False, repeat=5, port=None, delay=0.0, scheme=''):
    check_set = one_two(url, verbose, repeat, port, delay)
    log = generate()
    log.update({
        'result': 'Success' if check_set[0] else 'Failed after 5 %1.2f-second separated attempts' % SOCKET_TIMEOUT,
        'address': (scheme + '://' if scheme != '' else '') + check_set[1],
        'port': check_set[2],
        'error': check_set[3]
    })

    if '-p' in sys.argv:
        sys.stdout.write(json.dumps(log, sort_keys=True, indent=4) + '\n')
    else:
        sys.stdout.write(json.dumps(log, sort_keys=True))
