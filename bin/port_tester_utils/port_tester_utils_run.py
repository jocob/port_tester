import socket

SOCKET_TIMEOUT = 1.0

def set_socket_timeout(value):
    global SOCKET_TIMEOUT
    SOCKET_TIMEOUT = value


def run(url, port, verbose=False):
    try:
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.settimeout(SOCKET_TIMEOUT)
        result = sock.connect_ex((url, port))
    except Exception as e:
        return (False, url, port, e.message)

    if result == 0:
        if verbose:
            print('URL and port successfully connected!')
        return (True, url, port, 'None')
    else:
        if verbose:
            print('URL and port failed to connect')
        return (False, url, port, 'None')
