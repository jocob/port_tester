import datetime
import random
import re
import time


def get_unique_id():
    if '__run_id' not in globals():
        globals()['__run_id'] = [0]
    else:
        return globals()['__run_id'][0]

    global __run_id
    __run_id = globals()['__run_id']

    char = re.compile(r'[A-Za-z]')

    ranges = [ord('A'), ord('Z'), ord('a'), ord('z')]

    mini = ranges[0]
    maxi = ranges[-1]

    for r in ranges:
        if r < mini:
            mini = r
        if r > maxi:
            maxi = r

    chars = []

    while mini <= maxi:
        if char.match(chr(mini)):
            chars.append(chr(mini))
        mini += 1

    code = [random.choice(chars), random.choice(chars), random.choice(chars)]
    __run_id[0] = ''.join(code) + str(int(time.mktime(datetime.datetime.timetuple(datetime.datetime.now()))))

    return __run_id[0]
