import urlparse

PORT_DEFAULTS = {
    'file': 0,
    'ftp': 21,
    'gopher': 70,
    'hdl': 2641,
    'http': 80,
    'https': 443,
    'imap': 143,
    'mailto': 0,
    'mms': 651,
    'news': 119,
    'nntp': 119,
    'prospero': 191,
    'rsync': 873,
    'rtsp': 554,
    'rtspu': 554,
    'sftp': 115,
    'shttp': 443,
    'sip': 5060,
    'sips': 5061,
    'snews': 563,
    'svn': 3690,
    'svn+ssh': 22,
    'telnet': 23,
    'wais': 210,
}


def parse(url, verbose=False):
    purl = urlparse.urlparse(url)

    addr = purl.netloc if purl.netloc != '' else url
    port = purl.port
    schm = purl.scheme

    if verbose:
        if addr != '':
            print('Testing connection for ' + addr)
        else:
            print('Testing connection for ' + url)

    if port is None and schm != '':
        port = PORT_DEFAULTS[schm]
        if verbose:
            print("URL doesn't have a port, testing with port %d instead" % port)
    elif port is None:
        try:
            if ':' in addr:
                port = int((''.join(url.replace('https://', '').replace('http://', '').split(':')[1:])).split('/')[0])
            else:
                raise ValueError
        except ValueError:
            port = 443 if ('https' in url) else (80 if ('http' in url) else None)
            if verbose:
                print("Couldn't get port from URL, using %d instead" % port if port is not None else '(None)')
    addr = addr.split(':')[0]

    return addr, port, schm
