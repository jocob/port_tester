import time
from port_tester_utils_run import run, set_socket_timeout as run_timeout


def set_socket_timeout(value):
    run_timeout(value)


def one_two(url, verbose=False, repeat=5, port=None, delay=0.0):
    time.sleep(delay)
    result = run(url, port, verbose)

    if result[0]:
        return result
    elif repeat > 0:
        if verbose:
            print('Failed attempt')
        repeat -= 1
        return one_two(url, verbose, repeat, port, delay)
    else:
        return (False, url, port, 'None')
