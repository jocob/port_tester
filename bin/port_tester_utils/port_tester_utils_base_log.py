import datetime
import time
from port_tester_utils_group_id import get_unique_id


RUN_ID = get_unique_id()


def generate():
    return {
        'timestamp': str(datetime.datetime.now()),
        '_time': str(int(time.mktime(datetime.datetime.timetuple(datetime.datetime.now())))),
        'run_id': RUN_ID,
        'result': '',
        'address': '',
        'port': '',
        'error': ''
    }
